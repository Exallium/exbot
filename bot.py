from ircutils import bot
import re
import lxml.html
import commands

AWESOME = ["penguins", "cake", "cocaine"]
SITEEND = ["com", "org", "edu", "gov", "it", "ca"]
FILEEXT = ["jpg", "jpeg", "gif", "pdf", "tif", "png"]

COMMANDS = {
        ".ncix": commands.ncix_search,
        ".egg": commands.newegg_search,
        ".title": commands.print_title,
        ".calc": commands.google_calc,
        }

class ExalliumBot(bot.SimpleBot):
    def on_channel_message(self, event):
        message = event.message.split()
        
        awesome = []
        for msg in message:
            try:
                for aw in AWESOME:
                    p = re.compile(r'.*%s.*' % aw)
                    if p.match(msg.lower()):
                        awesome.append(aw)
            except:
                pass

        # If there's no message, we ignore
        if len(message) == 0:
            return

        # If we matched an awesome word, we shout it out.
        if awesome:
            self.send_message(event.target, "I fuckin love %s!" %
                    awesome[0])

        try:
            # We try to execute a command here
            body = ' '.join(message[1:])
            msg = COMMANDS[message[0]](self, body)

            if len(msg) != 0:
                self.send_message(event.target, msg)

        except:
            # Likely a key error, don't worry about it.
            pass

        finally:
            # this bit should happen irregardless every message
            p = re.compile(r'^http://.*')
            x = [msg for msg in message if p.match(msg)]

            for x in x:
                for ext in FILEEXT:
                    if x.endswith(".%s" % ext):
                        self.last_title = "%s image" % ext.upper()
                        return

                for end in SITEEND:
                    if x.endswith(end):
                        x += '/'

                last_title = lxml.html.parse(x).find(".//title")

                if last_title is not None: 
                    self.last_title = last_title.text 
                else:
                    self.last_title = "No title available"

if __name__ == "__main__":

    exallium = ExalliumBot("exbot")
    exallium.connect("irc.freenode.net", channel = ['#reddit-gamingpc'])
    exallium.start()

