import urllib
import re

CALC_BASE = "http://www.google.com/ig/calculator?"

def getDict(result):
    data_string = result.replace("{","").replace("}","")
    reg = re.compile(r'.*(?P<key>^[A-Za-z0-9]+): (?P<value>.*)')
    matches = [reg.match(data) for data in data_string.split(',')]
    data_dict = {"%s" % match.group(1): "%s" % match.group(2) for match in matches}
    return data_dict

def sendQuery(query): 
    query = urllib.urlencode({'q': '%s' % query})
    p = urllib.urlopen(CALC_BASE + "hl=en&%s" % query)
    return getDict(p.read())

