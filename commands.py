import re
import calc

def ncix_search(bot, body):
    return google_search(body, "ncix.ca")

def newegg_search(bot, body):
    return google_search(body, "newegg.com")

def google_search(body, site):
    return ".g %s site:%s" % (body, site)

def print_title(bot, body):
    try:
        return bot.last_title
    except AttributeError:
        return "No links yet"

def google_calc(bot, body):
    result = calc.sendQuery(body)
    if result['error'] == '""':
        import urllib
        rhs = result['rhs']
        rhs = urllib.unquote(rhs[1: len(rhs) - 1])
        rhs = rhs.replace('\\x26#215;', 'x')
        p = re.compile(r'.*\\x3csup\\x3e(\d+)\\x3c.sup\\x3e*')
        m = p.match(rhs)

        if m is not None:
            rhs = rhs.replace('\\x3csup\\x3e%s\\x3c/sup\\x3e' % m.group(1),
                    ' ^ %s' % m.group(1))
            lhs = result['lhs']
            return "%s = %s" % (lhs[1:len(lhs-1)] if len(lhs) < 20 else "", 
                    rhs)
        else:
            return "Can't answer that"
